# firstday
Personal notes on required development tools on first day at PRE  

## Required tools
| LEDdriven         |
|-------------------|
| Altium 365        |
| Gitlab            |
| MPLAB X IDE       |
| MPLAB XC16        |
| Slack             |
| SVN               |
| Microsoft WSL     |
| Visual code       |
| PCAN view         |

## Installation of tools

### Altium 365
Altium 365 does not require an installation as new design files can be opened in the browser. Exception for LEDdriven projects as they are not imported in Altium 365.  
Altium onboarding courses are provided free by Altium.  
Altium license and workspace is managed by Hans Peek. Both license and workspace access can be requested via Hans.

### Gitlab
Generate SSH keys and import them into your account on gitlab. Howto can be found online.

## Common tools
### PRE Bootloader
During startup of the MCU a bootloader will run for 10 seconds and then start (if present) the application software. The source of the bootloader can be found in Gitlab (PREBootloader).

#### CANopen identity
The bootloader uses the CANopen ID set in the EEPROM. The CANopen ID is determined by the application so on first start it will use the default ID of 0x7F. If for some reason the identity is not correct the EEPROM identity can be cleared via the following bootloader command:
```67F 23 00 20 02 07 00 00 00```
